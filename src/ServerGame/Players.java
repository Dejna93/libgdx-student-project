package ServerGame;

import java.util.ArrayList;


import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;



public class Players {
	
	private ArrayList<PlayerData> players = new ArrayList<PlayerData>();
	private MapGame map = new MapGame();
	public Players() { 
		
	}
	public Players(ArrayList<PlayerData> newplayers)
	{
		players = new ArrayList<PlayerData>(newplayers);
	}
	
	public void add(int ID,String username,Vector2 pos)
	{
		players.add(new PlayerData(ID,username,pos));
	}
	public void update(int port, Vector2 move, MapGame map)
	{
		this.map = map;
		for (int i =0 ; i < players.size(); i++)
		{		
			Rectangle futurePosition = new Rectangle(players.get(port).getPosition().x + move.x , players.get(port).getPosition().y + move.y,32,32);
			if (futurePosition.overlaps(players.get(i).getBounds()) && port!=i)
			{
				System.out.println("Kolizja");
				move = new Vector2(0,0);
				break;
			}
		}
		for (int i=0 ; i < map.getMap().size() ; i++)
		{
			Rectangle futurePosition = new Rectangle(players.get(port).getPosition().x + move.x , players.get(port).getPosition().y + move.y,32,32);
			if (futurePosition.overlaps(map.getTile(i).getBounds()) && (map.getTile(i).getOptions().equals("block") || map.getTile(i).getOptions().equals("shotable")) )
			{
				System.out.println("Kolizja");
				move = new Vector2(0,0);
				break;
			}
		}
		players.get(port).move(move);
	
	}
	public int size()
	{
		return players.size();
	}
	public ArrayList<PlayerData> getPlayers()
	{
			return players;
	}
	public Vector2 getPlayerPosition(int ID)
	{
		if( ID <players.size())
			return players.get(ID).getPosition();
		else
			return null;
	}

	public void remove(int ID) {
		// TODO Auto-generated method stub
		players.remove(ID);
		System.err.println("usuniety player"+ ID + players.size());
	}
}
