package Packet;

import java.io.Serializable;
import java.util.ArrayList;

import ServerGame.MapGame;
import Tiles.Tile;

public class PacketMap implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1813471245174344671L;

	private MapGame mapGame;
	
	
	public PacketMap(MapGame map) {
		// TODO Auto-generated constructor stub
		this.mapGame = map;
	}
	public void setNewMap(MapGame map)
	{
		this.mapGame = map;
	}
	public MapGame getMap()
	{
		return mapGame;
	}
	public ArrayList<Tile> getTiles()
	{
		return mapGame.getMap();
	}
}
