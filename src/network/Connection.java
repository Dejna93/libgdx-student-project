package network;

import handlers.BulletsHandler;

import java.io.BufferedInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;


import Packet.PacketBullets;
import Packet.PacketDisconnect;
import Packet.PacketLoged;
import Packet.PacketLogin;
import Packet.PacketMap;
import Packet.PacketMove;
import Packet.PacketShoot;
import Packet.PacketTileUpdate;
import Packet.PacketUpdate;

import Packet.Utils;
import ServerGame.MapGame;
import ServerGame.PlayerData;


import com.badlogic.gdx.math.Vector2;


import entities.BulletMulti;

public class Connection implements Runnable {
	private Socket socket = null;

	private Thread thread = null;
	private ConnectionThread client = null;
	private ObjectInputStream streamIn;
	private ObjectOutputStream streamOut;

	private boolean isLogin = false;
	public boolean isHandling = false;
	private boolean getLoged = false;

	private int IDClient = -1;


	private ArrayList<PlayerData> players = new ArrayList<PlayerData>();

	private MapGame map = new MapGame();
	private BulletsHandler bulletHandler = new BulletsHandler();

	public Connection() {

	}

	public Connection(String ip, int port) {
		// TODO Auto-generated constructor stub
		try {

			socket = new Socket(ip, port);

			open();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void send(Object object) {
		try {
			streamOut.writeObject(object);
			streamOut.reset();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static ObjectInputStream createInputStream(Socket socket)
			throws IOException {
		InputStream stream = socket.getInputStream();
		BufferedInputStream buffer = null;

		try {
			buffer = new BufferedInputStream(stream);

			return new ObjectInputStream(buffer);
		} catch (IOException ex) {
			if (buffer != null)
				Utils.close(buffer);

			stream.close();

			throw ex;
		}
	}

	public ObjectOutputStream createOutputStream(Socket socket)
			throws IOException {
		OutputStream stream = socket.getOutputStream();

		try {
			return new ObjectOutputStream(stream);
		} catch (IOException ex) {
			stream.close();

			throw ex;
		}
	}

	public void open() {
		try {
			streamOut = this.createOutputStream(socket);

			if (thread == null) {
				client = new ConnectionThread(this, socket);
				thread = new Thread(this);
				thread.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void handle(Object obj) {
		// System.out.println(obj.toString());

		if (obj.getClass() == PacketLoged.class) {
			PacketLoged pack = (PacketLoged) obj;
			// players = pack.getPlayers();
			getLoged = true;
			handleLogin(pack);

		} else if (obj.getClass() == PacketUpdate.class) {
			PacketUpdate packet = (PacketUpdate) obj;
			players = packet.getPlayers();
		} else if (obj.getClass() == PacketMap.class) {
			PacketMap packetMap = (PacketMap) obj;
			map = packetMap.getMap();
		} else if (obj.getClass() == PacketBullets.class) {
			PacketBullets packet = (PacketBullets) obj;

			bulletHandler.add(packet);
		} else if (obj.getClass() == PacketTileUpdate.class) {
			PacketTileUpdate packet = (PacketTileUpdate) obj;
			System.out.println("tile " + packet.getID() + " "
					+ packet.getType() + " " + packet.getOption());
			map.setTile(packet.getBounds(), packet.getType(),
					packet.getOption(), packet.getID());

		}

	}

	public int getID() {
		for (int i = 0; i < players.size(); i++) {

			if (players.get(i).getID() == socket.getLocalPort()) {
				return i;
			}
		}
		return 0;
	}

	public Vector2 getPlayerPosition(int ID) {
		System.err.println(ID + " " + players.size());
		return players.get(ID).getPosition();
	}

	public ArrayList<PlayerData> getPlayers() {
		if (players.size() > 0)
			return players;
		else {
			players.add(new PlayerData(-1, "Ss", new Vector2(0, 0)));
			return players;
		}
	}

	public PlayerData getPlayer(int ID) {
		return players.get(ID);
	}

	public BulletsHandler getBullets() {
		return bulletHandler;
	}

	public void handleLogin(PacketLoged packet) {

		this.isLogin = packet.getLogin();
		// this.pos = packet.getPosition();
		players = packet.getPlayers();
		map = packet.getMap();

	}

	public MapGame getMap() {
		return map;
	}

	public void sendLogin(String username, String password) {
		// if (!username.isEmpty() && !password.isEmpty())
		// {
		PacketLogin login = new PacketLogin(socket.getPort());
		login.setUsername(username);
		login.setPassword(password);

		send(login);
		/*
		 * try { System.err.println(login.getUsername() + " " +
		 * login.getPassword() + " " + login.toString());
		 * streamOut.writeObject(login); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */

		// }

	}

	public void sendShoot(BulletMulti bullet) {

		// System.out.println("Shoot" + "" + bullet.getID() + " " +
		// bullet.getPosition().x + " "+ bullet.getPosition().y)
		send(new PacketShoot(bullet));
	}

	public boolean isLogin() {
		return isLogin;
	}

	public void sendMove(Vector2 pos) {
		// TODO Auto-generated method stub
		send(new PacketMove(socket.getLocalPort(), pos));

	}

	public void sendDisconnect(boolean state) {
		send(new PacketDisconnect(true));
		/*
		 * try { streamOut.writeObject(new PacketDisconnect(true)); } catch
		 * (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}

	public void stop() throws IOException {
		if (socket != null)
			socket.close();
		if (streamIn != null)
			streamIn.close();
		if (streamOut != null)
			streamOut.close();
	}

	public void setLogin(boolean isLogin) {
		this.isLogin = isLogin;
	}

	public boolean isConnected() {
		return socket.isConnected();
	}

	public boolean getLoginPacket() {
		// TODO Auto-generated method stub
		return getLoged;
	}

	public void setLoged(boolean state) {
		getLoged = state;
	}

	public int IDClient() {
		return IDClient;
	}

	public int getPort() {
		// TODO Auto-generated method stub
		return socket.getPort();
	}
}
