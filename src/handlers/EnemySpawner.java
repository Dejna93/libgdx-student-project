package handlers;

import java.util.ArrayList;
import java.util.Random;

import Tiles.Tile;
import Utils.*;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import entities.Tank;

public class EnemySpawner {
	
	private int level;
	private SpriteBatch sb;
	ArrayList<Tank> tanks = new ArrayList<Tank>();
	Rectangle playerBounds;
	public void setSprite(SpriteBatch sb)
	{
		this.sb = sb;
	}
	public EnemySpawner(int level,Rectangle playerBounds,SpriteBatch sb) {
		// TODO Auto-generated constructor stub
		this.sb = sb;
		this.level = level;
		this.playerBounds = playerBounds;
		spawn(5);
	}
	
	public void spawn(int count)
	{
		for (int i = 0 ; i < count ; i++)
		{
			tanks.add(new Tank(playerBounds, getVector()));
		}
	}
	
	public void update(Rectangle playerBounds, Vector2 position, ArrayList<Tile>tileMap)
	{
		for (int i=0 ; i < tanks.size() ; i++)
		{
		
			tanks.get(i).updateMap(tileMap);
			tanks.get(i).update(position,playerBounds,getList(i));
	
			
		}
	}
	public void remove(int id)
	{
		if(tanks.size() < id)
		{
			tanks.remove(id);
		}
	}
	private ArrayList<TankUtil> getList(int id)
	{
		ArrayList<TankUtil> tank = new ArrayList<TankUtil>();
		for(int i=0 ; i < tanks.size(); i++)
		{
			if(id!=i)
			{

				tank.add(new TankUtil(tanks.get(i).getBounds(),tanks.get(i).getDirection() ));
			}
		}
		return tank;
	}
	private Vector2 getVector()
	{
		Vector2 position = new Vector2();
		Random rand = new Random();
		position.x = rand.nextInt(640);
		position.y = rand.nextInt(480);
		System.out.println(position.x + " " + position.y);
		return position;
	}
	
	public void draw()
	{
		for(int i=0; i < tanks.size();i++)
		{
			tanks.get(i).getSprite().draw(sb);
			tanks.get(i).drawBullets(sb);
		}

	}
	
}
