package XMLPraser;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;

import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.*;

import Tiles.Tile;
import Utils.TypeTile;

public class ReaderXML implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8814728525051058531L;

	private TypeTile types = new TypeTile();
	private ArrayList<Tile> tiles = new ArrayList<Tile>();
	private HashMap<Integer, String> option = new HashMap<Integer, String>();
	private ArrayList<TypeTile.typeTile> typeTile = new ArrayList<TypeTile.typeTile>();
	private ArrayList<Vector2> spawnPoints = new ArrayList<Vector2>();
	// public ArrayList<Element> tile = new ArrayList<XmlReader.Element>();
	private String filepath;

	public class Settings {

		private int tilewidth;
		private int tileheight;

		public Settings() {
			// TODO Auto-generated constructor stub
		}

		public void setWidth(int width) {
			this.tilewidth = width;
		}

		public void setHeight(int height) {
			this.tileheight = height;
		}

		public int getWidth() {
			return tilewidth;
		}

		public int getHeight() {
			return tileheight;
		}
	}

	public Vector2 getSpawn(int id) {
		return spawnPoints.get(id);
	}

	public ArrayList<TypeTile.typeTile> getTypeTile() {
		return typeTile;
	}

	public ArrayList<Vector2> getSpawnPoints() {
		return spawnPoints;
	}

	public ReaderXML(String filepath) {
		this.filepath = filepath;
	}

	public ArrayList<Tile> loader(boolean isSingle) throws IOException {
		XmlReader reader = new XmlReader();
		Settings settings = new Settings();
		Element root = reader.parse(new FileHandle(filepath));

		Element data = root.getChildByName("data");
		Element options = root.getChildByName("option");

		Element typeT = root.getChildByName("typetile");
		Element spawnpoints = root.getChildByName("spawnpoint");

		// for (int i=0 ; i < typeT.getChildCount() ; i++)
		// {
		// typeTile.add(types.getType(typeT.getChild(i).getAttribute("enum")));
		// System.out.println("type " + typeTile.get(i).name());
		// }
		for (int i = 0; i < spawnpoints.getChildCount(); i++) {
			spawnPoints.add(new Vector2(spawnpoints.getChild(i)
					.getIntAttribute("x"), spawnpoints.getChild(i)
					.getIntAttribute("y")));
			System.out.println("SpawnPoints " + spawnPoints.get(i).x + " "
					+ spawnPoints.get(i).y);
		}
		for (int i = 0; i < options.getChildCount(); i++) {
			option.put(options.getChild(i).getIntAttribute("type"), options
					.getChild(i).getAttribute("option"));
			// System.out.println(i + " "
			// +options.getChild(i).getAttribute("type") + " "+
			// options.getChild(i).getAttribute("option"));
		}

		for (int i = 0; i < data.getChildCount(); i++) {
			Element tmp = data.getChild(i);
			int type = tmp.getIntAttribute("type");
			tiles.add(new Tile(tmp.getIntAttribute("x"), tmp
					.getIntAttribute("y"), tmp.getIntAttribute("type"), option,
					isSingle));

		}
		System.out.println(tiles.size() + " koniec ");

		return tiles;
	}

	public String getType(int id) {
		return option.get(id);
	}
	/*
	 * public static void main(String[] args) throws IOException { new
	 * ReaderXML();
	 * 
	 * System.exit(0); }
	 */

}
