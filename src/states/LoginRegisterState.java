package states;



import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;


import handlers.DatebaseHandler;
import handlers.GameStateManager;

public class LoginRegisterState extends GameState {

	Skin skin;
	Stage stage;

	private Table table;
	
	private TextButton registerButton;

	private TextField userField;
	private TextField passwordField;
	private TextField passwreapetField;
	
	private DatebaseHandler db;
	private Label userLabel;
	private Label passLabel;
	private Label pass2Label;
	
	public LoginRegisterState(GameStateManager gSManager) {
		super(gSManager);
		
		stage = new Stage();

		skin = new Skin(Gdx.files.internal("assets/json/uiskin.json"));
		
		db = new DatebaseHandler();
		
		registerButton = new TextButton("Rejestracja",skin,"default");
		userField  = new TextField("", skin);
		passwordField = new TextField("", skin);
		passwreapetField = new TextField("", skin);
		passwordField.setPasswordMode(true);
		passwordField.setPasswordCharacter('*');
		passwreapetField.setPasswordMode(true);
		passwreapetField.setPasswordCharacter('*');

		table = new Table();
		table.setFillParent(true);
		
		userLabel= new Label("Username", skin);
		passLabel = new Label("Password", skin);
		pass2Label = new Label("Repeat", skin);
		
		
        Gdx.input.setInputProcessor(stage);// Make the stage consume events
      
   
        stage.addActor(table);
       
		registerPanel();
	}

	private void registerPanel()
	{
		table.debug();
		table.add(userLabel).width(100);
		table.add(userField);
		table.row();
		table.add(passLabel);
		table.add(passwordField);
		table.row();
		table.add(pass2Label);
		table.add(passwreapetField);
		table.row();
		table.add(registerButton).colspan(2);
		stage.addActor(table);
	}
	
	@Override
	public void handleInput() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act();
		stage.draw();
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
