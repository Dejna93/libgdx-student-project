package ServerGame;

import handlers.DatebaseHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import Packet.*;

public class Server implements Runnable {

	// private ServerThread clients[] = new ServerThread[2];
	private ArrayList<ServerThread> clients = new ArrayList<ServerThread>();
	// private HashMap<Integer, ServerThread> clients = new HashMap<Integer,
	// ServerThread>();
	// private HashMap<Inet4Address, ServerThread> clients = new
	// HashMap<Inet4Address, ServerThread>();

	private ServerSocket server = null;
	private Thread thread = null;
	private int clientCount = 0;

	private Players playersConnected = new Players();
	private DatebaseHandler db;
	private int port = 7777;
	private MapGame map = new MapGame();
	private ShootManager shootManager = new ShootManager();

	public Server(int port) {

		runServer();

	}

	public void runServer() {
		try {
			System.out
					.println("Binding to port " + port + ", please wait  ...");
			server = new ServerSocket(port);
			System.out.println("Server started: " + server);
			start();
			db = new DatebaseHandler();

		} catch (IOException ioe) {
			System.out.println("Can not bind to port " + port + ": "
					+ ioe.getMessage());
		}
	}

	public void run() {

		while (thread != null) {
			try {
				System.out.println("Waiting for a client ...");
				addThread(server.accept());
			} catch (IOException ioe) {
				System.out.println("Server accept error: " + ioe);
			}
		}
	}

	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	public void stop() {
		if (thread != null) {
			thread.stop();
			thread = null;
		}
	}

	public void handle(Object obj, int port) {
		// System.err.println(obj.getMsg() + " " + obj.getPort());
		// System.err.println("pakiet odebrany od portu " + obj.toString());
		if (obj.getClass() == PacketMove.class) {
			System.out.println("packetmove" + getID(port));
			handleMove(obj, port);
		}
		if (obj.getClass() == PacketLogin.class) {
			System.out.println("packetlogin " + getID(port) + " " + port);
			PacketLogin log = (PacketLogin) obj;
			handleLogin(log, port);
		}
		if (obj.getClass() == PacketMap.class) {
			System.out.println("Map update");
			handleMapUpdate((PacketMap) obj);
		}
		if (obj.getClass() == PacketDisconnect.class) {

			// if(getID(port) != -1)
			// {
			// clients[getID(port)].setLoop(false);
			// remove(getID(port));
			// }
		}
		if (obj.getClass() == PacketChat.class) {

		}
		if (obj.getClass() == PacketShoot.class) {
			PacketShoot packet = (PacketShoot) obj;
			handleShoot(packet);
		}

	}

	private void handleMapUpdate(PacketMap packetMap) {
		// TODO Auto-generated method stub
		// zrobie tak by pakiet z update map otrzymywal tylko danego tile z
		// aktualizacja multicast do wszystkich
		// logowanie i wysylanie mapy klient otrzymuje mape i texturuje
		// wszystkie
		// przy kolizje liczone na serwerze
	}

	private int getID(int port) {
		for (int i = 0; i < clientCount; i++)
			if (clients.get(i).getPort() == port)
				return i;
		System.err.println("Port nie znaleziony kod " + "-1");
		return -1;
	}

	public synchronized void broadcast(Object object) {

		for (int i = 0; i < playersConnected.size(); i++) {
			// System.out.println(object.toString());
			clients.get(i).send(object);
		}
	}

	public void handleLogin(PacketLogin packetLogin, int port) {
		System.err.println(packetLogin.getID() + " "
				+ packetLogin.getUsername() + " " + packetLogin.getPassword());

		if (db.isContain(packetLogin.getUsername(), packetLogin.getPassword())) {
			System.out.println("zalogowany" + " send ");
			playersConnected.add(port, packetLogin.getUsername(),
					map.getSpawnPosition(getID(port))); // do testow
			clients.get(getID(port)).send(
					new PacketLoged(getID(port), true, playersConnected
							.getPlayers(), map));

			// playerdata.put(packetLogin.getID(), new PlayerData(random()));
		} else {
			System.out.println("blad ");
			clients.get(getID(port)).send(new PacketLoged(false));

		}

		// System.out.println(playerdata.get(packetLogin.getID()).getPosition().x
		// +playerdata.get(packetLogin.getID()).getPosition().y );
	}

	public void handleShoot(PacketShoot packet) {
		shootManager.add(packet.getBullet());

	}

	public void handleMove(Object obj, int port) {
		// TODO Auto-generated method stub
		PacketMove packet = (PacketMove) obj;

		playersConnected.update(getID(port), packet.getPosition(), map);
		for (int i = 0; i < playersConnected.size(); i++) {
			System.out.println("Klient  " + i + " "
					+ playersConnected.getPlayers().get(i).getID() + " "
					+ playersConnected.getPlayers().get(i).getPosition().x
					+ " "
					+ playersConnected.getPlayers().get(i).getPosition().y);
		}
		broadcast(new PacketUpdate(playersConnected.getPlayers()));// /ew
																	// PlayerData(getID(port),"S",playersConnected.getPlayerPosition(getID(port)))));

	}

	public synchronized void remove(int ID) {

		// try{
		int id = getID(ID);
		playersConnected.remove(id);
		try {
			clients.get(id).close();
			clients.remove(id);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		broadcast(new PacketUpdate(playersConnected.getPlayers()));
		clientCount = playersConnected.size();
		System.out.println("Usuniety klient" + ID + " " + " "
				+ playersConnected.size());
		// }catch(IOException ioe)
		// {
		// ioe.printStackTrace();
		// }

	}

	public void update() {
		shootManager.update(map, this);
		if (shootManager.isShoot() == true) {

			broadcast(new PacketBullets(shootManager));
		}
	}

	private void addThread(Socket socket) {
		// if (clientCount < 2) {
		System.out.println("Client accepted: " + socket + clients.size());

		clients.add(new ServerThread(this, socket));

		try {
			clients.get(clientCount).open();
			System.err.println("open");
			clients.get(clientCount).start();
			System.err.println("start");
			clientCount++;

		} catch (IOException ioe) {
			System.out.println("Error opening thread: " + ioe + " "
					+ clientCount);
		}
		// } else
		// System.out.println("Client refused: maximum " + " reached.");
	}

}
