package states;


import java.util.ArrayList;
import java.util.Random;


import handlers.EnemySpawner;
import handlers.GameStateManager;
import AssetsLoader.Assets;
import Tiles.Tile;

import XMLPraser.ReaderXML;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;

import com.badlogic.gdx.graphics.OrthographicCamera;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import entities.Player;
import entities.Tank;

public class PlaySingle extends GameState implements Screen{

	private Stage stage;

	private Skin skin;
	private Label label ;

	private OrthographicCamera cam;

	private Player player;
	private boolean hit = false;
	private ShapeRenderer sR;
	private ArrayList<Tank> aTanks;
	private ArrayList<Tile> tileMap = new ArrayList<Tile>();
	private EnemySpawner enemySpawner;
	
	TextureAtlas atlas;
	private ReaderXML map = new ReaderXML("levels/level2.xml");
	
	 
	//--
	
	
	public PlaySingle(GameStateManager gSManager) {
		super(gSManager);
		//gen();
		try{
		aTanks = new ArrayList<Tank>();
		cam = new OrthographicCamera();
		cam.setToOrtho(false,WIDTH,HEIGHT);
		cam.update();
		sR = new ShapeRenderer();
		stage = new Stage();
	//	skin = new Skin(Gdx.files.internal("assets/json/uiskin.json"));
		label = new Label("Score",Assets.getManager().get("assets/json/uiskin.json", Skin.class));

		label.setPosition(20,450);
		stage.addActor(label);

		tileMap = map.loader(true);
	
		//atlas = new TextureAtlas("levels/tileset.pack");
	/*	
		for (int i = 0 ; i < tileMap.size(); i++)
		{
			if (isBlock(i))
			{
				aBlocks.add(tileMap.get(i).getBounds());
			}
				
		}*/
		player = new Player(tileMap);
		
		enemySpawner = new EnemySpawner(1, player.getBounds(), sb);
		
		}catch ( Exception e )
		{
			e.printStackTrace();
		}
		
	}
	
	private boolean isBlock(int id)
	{
		if (tileMap.get(id).getOptions() == "block")
			return true;
		return false;
	}
	

	
	@Override
	public void handleInput() {
		// TODO Auto-generated method stub
		if (Gdx.input.isKeyPressed(Keys.ESCAPE))
		{
			gSManager.popState();
			gSManager.pushState(gSManager.MENU);
		}
		if (Gdx.input.isKeyPressed(Keys.UP))
		{	
			player.setUp();
		}
		else if (Gdx.input.isKeyPressed(Keys.DOWN))
		{
			player.setDown();
		
		}
		else if (Gdx.input.isKeyPressed(Keys.LEFT))		
		{
			player.setLeft();
		}
		else if (Gdx.input.isKeyPressed(Keys.RIGHT))
		{
			player.setRight();	
		}
		
		if (Gdx.input.isKeyPressed(Keys.SPACE)) //&& lastShoot > 0.2f)
		{
			if (!player.isShooting())
				{
				player.shoot();
				player.setShoot(true);
				}
			//	System.out.println("shooting");
			//shoot.shoot(player.getPosition(),player.getPlayerDirect());
			//lastShoot = 0f;
		}
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		
		handleInput();
		player.updateCollision(tileMap);
		player.update();
		enemySpawner.update(player.getBounds(), player.getPosition(), tileMap);
		for (int i =0 ; i < tileMap.size();i++)
		{
			for (int j=0 ; j < player.bulletsize() ; j++)
			{
			//	System.out.println(player.getBoundsBullet(j).x +" "+ player.getBoundsBullet(j).y+ " " + player.getBoundsBullet(j).width + " " + player.getBoundsBullet(j).height);
				if (player.getBoundsBullet(j).overlaps(tileMap.get(i).getBounds())&& tileMap.get(i).getOptions().equals("shotable"))
				{
					player.removeBullet(j);
					tileMap.remove(i);		
					break;
				}
			}
		
		}
		
		//System.out.println(player.getBounds().x/layer.getTileWidth()+ " "+ player.getBounds().y/layer.getTileHeight());
	
		
	}

	@Override
	public void render() {
		

		cam.update();

		stage.act();
		stage.draw();
		
		draw();
		
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub

		sb.begin();

		for (int i=0; i< tileMap.size(); i++)
		{
		//	System.out.println(tileMap.get(i).getBounds().x + " " +tileMap.get(i).getBounds().y + " " + tileMap.get(i).getBounds().width + " " + tileMap.get(i).getBounds().height) ;
			tileMap.get(i).draw(sb);
		}
		for (int i=0; i < aTanks.size();i++)
		{
			aTanks.get(i).getSprite().draw(sb);
			aTanks.get(i).drawBullets(sb);
		}
		player.getSB().draw(sb);
		player.drawBullets(sb);
		
		enemySpawner.draw();
		sb.end();
	

	}
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	private int random()
	{
		Random rand = new Random();
		return rand.nextInt(500);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resize(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}
}

