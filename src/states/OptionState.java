package states;

import java.util.ArrayList;

import handlers.GameStateManager;

import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;


public class OptionState extends GameState{

	private Skin skin;
	private Stage stage;
	
	private Table table;
	private Table cont;
	private TextField chatInput;
	private TextButton chatSend;
	
	private List list;
	
	private ScrollPane scrollPane;

	ArrayList<String> msg = new ArrayList<String>();
	public OptionState(GameStateManager gSManager) {
		super(gSManager);
		// TODO Auto-generated constructor stub
		stage = new Stage();
		skin = new Skin(Gdx.files.internal("assets/json/uiskin.json"));
		
		table = new Table();

		cont = new Table();
		list = new List(skin);
		
		scrollPane = new ScrollPane(cont,skin);
	
		
		chatInput = new TextField("", skin);
		chatSend  = new TextButton("Send", skin);
		
		table.setFillParent(true);
		cont.align(Align.left);
	
		table.add(scrollPane).fill();
        Gdx.input.setInputProcessor(stage);// Make the stage consume events

        chatWindow();
 
		stage.addActor(table);
		
		table.debug();
		
		
	}

	public void chatWindow()
	{
		
		chatSend.addListener(new ChangeListener() {
			
	    	   public void changed (ChangeEvent event, Actor actor) {
	    		   Label label = new Label(validate(chatInput.getText()), skin);
	    		   label.setWrap(false);
	    		   System.err.println(label.getText());
	    		   cont.add(label);
	    		//msg.add(validate(chatInput.getText()));
	    		}
	       });
		
	
		table.row();
		table.add(chatInput);
		table.add(chatSend);
		table.row();
		
	}
	private String validate(String msg)
	{
		int size = msg.length();
		StringBuffer buffor = new StringBuffer();
		for (int i=0 ; i < msg.length(); i++)
		{
			if(i % 20 == 0 && i!=0)
			{
			buffor = new StringBuffer(msg);
			buffor.insert(i,"\n");
			msg = buffor.toString();
			}
		}
		
		return msg;
	}
	@Override
	public void handleInput() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		if (msg.size() > 5)
		{
			msg.remove(0);
		
		}	
		list.setItems(msg.toArray());
	
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		
	
		stage.act();
		stage.draw();
	
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
	//	sb.begin();
	//	sb.enableBlending();
	
	//	sb.end();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
