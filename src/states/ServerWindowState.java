package states;

import handlers.ServerStateManager;
import ServerGame.Server;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;


public class ServerWindowState extends ServerState{

	private Server server;
	private Stage stage;
	private Skin skin;	
	
	private Table table;
	private TextField portField;
	private TextButton accept;
	
	private Label portLabel;
	
	public ServerWindowState(ServerStateManager sStateManager) {
		super(sStateManager);
		
		skin = new Skin(Gdx.files.internal("assets/json/uiskin.json"));
		stage = new Stage();
		
		table = new Table();
		
		portField = new TextField("", skin);
		accept = new TextButton("Accept", skin);
		
		portLabel = new Label("Server port ", skin);
		table.setFillParent(true);
		// TODO Auto-generated constructor stub
		Gdx.input.setInputProcessor(stage);
		
		initMenu();
		handleInput();
	    stage.addActor(table);
	    
	}

	public void initMenu()
	{
		stage.clear();
		table.add(portLabel);
		table.add(portField);
		table.row();
		table.add();
		table.add(accept);
	}
	@Override
	public void handleInput() {
		// TODO Auto-generated method stub
		accept.addListener(new ChangeListener() {
			
	    	   public void changed (ChangeEvent event, Actor actor) {
	    		server = new Server(7777);
	    		}
	       });
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
	//	System.out.println("jestem");
		if(server!=null)
		server.update();
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act();
		stage.draw();
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
