package ServerGame;

import java.io.Serializable;

import Utils.Direction;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class PlayerData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4168716596929732412L;
	private Vector2 position;
	private String username;
	private int ID = -1;
	private Direction.direction direction;
	private Rectangle bounds = new Rectangle();
	
	public PlayerData(int ID,String username,Vector2 pos) {
		// TODO Auto-generated constructor stub
		this.ID = ID;
		this.username = username;	
		this.position = pos;
		bounds.set(position.x,position.y,32,32);
		setDirection(Utils.Direction.direction.Up);
	}
	public PlayerData(int ID,PlayerData data) {
		// TODO Auto-generated constructor stub
		this.position = data.getPosition();
		this.username = data.getUsername();
		this.ID  = ID;
		this.direction = data.direction;
	}
	public String getUsername()
	{
		return username;
	}
	public int getID()
	{
		return ID;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public Vector2 getPosition()
	{
		return position;
	}
	public void setPosition(Vector2 pos)
	{
		this.position = pos;
	}
	public void move(Vector2 move) {
		
		this.position.x += move.x;
		this.position.y += move.y;
		
		bounds.setPosition(position);
		
		setDirection(move);
	
	}
	public Rectangle getBounds()
	{
		return bounds;
	}
	private void setDirection(Vector2 move)
	{
		if (move.x < 0)
			setDirection(direction.Left);
		else if (move.x > 0)
			setDirection(direction.Right);
		else if (move.y > 0)
			setDirection(direction.Up);
		else if (move.y < 0 )
			setDirection(direction.Down);
	}
	public Utils.Direction.direction getDirection() {
		return direction;
	}
	public void setDirection(Direction.direction direction) {
		this.direction = direction;
	}
}
