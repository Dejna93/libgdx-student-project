package ServerGame;

import java.util.ArrayList;

import Packet.PacketTileUpdate;
import Utils.Direction.direction;

import com.badlogic.gdx.math.Vector2;

import entities.BulletMulti;

public class ShootManager {

	ArrayList<BulletMulti> bullets = new ArrayList<BulletMulti>();
	boolean isShoot = false;

	public ShootManager() {

	}

	public ShootManager(BulletMulti bullet) {
		// TODO Auto-generated constructor stub

	}

	public int size() {
		return bullets.size();
	}

	private boolean isHear(int port) {
		for (int i = 0; i < bullets.size(); i++) {
			if (port == bullets.get(i).getID())
				return true;
		}
		return false;
	}

	public void update(MapGame map, Server server) {

		for (int i = 0; i < bullets.size(); i++) {

			if (bullets.get(i).getPosition().x > 800
					|| bullets.get(i).getPosition().x < 0
					|| bullets.get(i).getPosition().y > 500
					|| bullets.get(i).getPosition().y < 0) {
				bullets.remove(i);
				isShoot = false;
				break;
			}

			bullets.get(i).update();

			for (int j = 0; j < map.size(); j++) {
				if (map.getTile(j).getOptions().equals("shotable")
						&& map.getTile(j).getBounds()
								.overlaps(bullets.get(i).getBounds())) {
					System.err.println(j + " block");
					bullets.remove(i);
					map.getTile(j).setType(1); // ground
					server.broadcast(new PacketTileUpdate(map.getTile(j), j));
					break;
				}
			}

			// System.out.println("Update bullet" + i +
			// bullets.get(i).getPosition().x + " " +
			// bullets.get(i).getPosition().y);

		}
	}

	public boolean isShoot() {
		return isShoot;
	}

	public ArrayList<BulletMulti> getBullets() {
		return bullets;
	}

	public Vector2 getPosition(int id) {
		return bullets.get(id).getPosition();
	}

	public direction getDirection(int id) {
		return bullets.get(id).getDirection();
	}

	public void add(BulletMulti bullet) {
		// TODO Auto-generated method stub
		if (!isHear(bullet.getID())) {
			System.out.println("Nie ma go, dodaje");
			isShoot = true;
			bullets.add(bullet);
		}
	}
}
