package entities;

import java.util.ArrayList;








import AssetsLoader.Assets;
import Tiles.Tile;
import Utils.Direction.direction;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;



public class Player extends Entity{


	//skierowanie czolgu
	private direction playerDirect;
	
	private boolean shoot = false;
	private Rectangle enemy;
	private ArrayList<Tile> tileMap;
	private ArrayList<Rectangle> aTanks = new ArrayList<Rectangle>();
	public float vx=0,vy=0;
	private ArrayList<Bullet> bullets = new ArrayList<Bullet>();

	public Player(ArrayList<Tile> tileMap) 	
	{
		setPosition(300,100);
		this.tileMap = tileMap;
		speed = 4;
	
		Texture text = Assets.getManager().get("assets/texture/player_up.png", Texture.class);
		
		sprite = new Sprite(text );
	//	this.aBlocks = aBlocks;
		
		setPlayerDirect(playerDirect.Up);

		bounds = new Rectangle(getX(),getY(),getWidth(),getHeight());
		enemy = new Rectangle();
		

	}
	public void setShoot(boolean state)
	{
		shoot = state;
	}
	public void  removeTank() {
			aTanks.clear();
	}
	private boolean isCollision(Rectangle enemy)
	{
		if (bounds.overlaps(enemy))
			return true;
		return false;
	}
	public int bulletsize()
	{
		return bullets.size();
	}
	public Sprite getSB()
	{
		return sprite;
	}
	public void updateCollision(ArrayList<Tile> tileMap)
	{
		this.tileMap = tileMap;
		
	}
	public void getTanks(ArrayList<Tank> aEnemies)
	{
		for (int i=0 ; i < aEnemies.size(); i++)
			this.aTanks.add(aEnemies.get(i).getBounds()) ;
	}
	public void setLeft()
	{
		Rectangle future = new Rectangle(getX()-speed,getY(),getWidth(),getHeight());
		String op = "block";
		for (int i =0 ; i < tileMap.size(); i++)
		{
		
			if (future.overlaps(tileMap.get(i).getBounds()) && (tileMap.get(i).getOptions().equals("block") || tileMap.get(i).getOptions().equals("shotable")))
			{
				System.out.println(tileMap.get(i).getOptions());
				move(speed, 0);
				break;
			}

			
		}
		move(-speed , 0);
		setPlayerDirect(playerDirect.Left);
		sprite.setRotation(90);
	


	
	}
	public void setRight()
	{

	Rectangle future = new Rectangle(getX()+speed,getY(),getWidth(),getHeight());
		
		for (int i =0 ; i < tileMap.size(); i++)
		{
			System.err.println(tileMap.get(i).getOptions());
			if (future.overlaps(tileMap.get(i).getBounds()) && (tileMap.get(i).getOptions().equals("block") || tileMap.get(i).getOptions().equals("shotable")))
			{
				move(-speed , 0);
				break;
			}
			
		}
	
		move(speed , 0);
		setPlayerDirect(playerDirect.Right);
		sprite.setRotation(-90);
		
	}
	public void setUp()
	{
	Rectangle future = new Rectangle(getX(),getY()+speed,getWidth(),getHeight());
		
		for (int i =0 ; i < tileMap.size(); i++)
		{
			System.err.println(tileMap.get(i).getOptions());
			if (future.overlaps(tileMap.get(i).getBounds()) && (tileMap.get(i).getOptions().equals("block") || tileMap.get(i).getOptions().equals("shotable")))
			{
				move(0,-speed);
				break;
			}
		}
		move(0,speed );
		setPlayerDirect(playerDirect.Up);
		sprite.setRotation(0);

	}
	public void setDown()
	{
		Rectangle future = new Rectangle(getX(),getY()-speed,getWidth(),getHeight());
	
		for (int i =0 ; i < tileMap.size(); i++)
		{
			System.err.println(tileMap.get(i).getOptions());
			if (future.overlaps(tileMap.get(i).getBounds())  && (tileMap.get(i).getOptions().equals("block") || tileMap.get(i).getOptions().equals("shotable")))
			{
				move(0,speed);
				break;
			}
		}
		move(0,-speed );
		setPlayerDirect(playerDirect.Down);
		sprite.setRotation(180);

		
		
	
	}
	public int getBulletHight()
	{ 
		return 4;
	}
	public Vector2 getBulletPosition(int id)
	{
		try{
		return bullets.get(id).getPositon();
		}catch(Exception e)
		{
			return new Vector2(-1,-1);
		}
	}
	protected void move(float vx ,float vy)
	{
		this.x += vx;
		this.y += vy;
			
		validposition();

}
	
	public void update()
	{	

	for  (int i =0 ; i < bullets.size() ; i++)
	{
		bullets.get(i).update(getPosition());
		shoot = bullets.get(i).getShoot();
	}
	
	float tempx= x;
	float tempy =y;
	
	x +=vx;
	y +=vy;
	
	bounds.x = x;
	bounds.y = y;	
	sprite.setPosition(x,y);

	}
	private void validposition()
	{

		if (x < 0 )
		{
			x=0;
			vx=0;
		}
		if(x>WIDTH-getWidth())
		{
			x=WIDTH-getWidth();
			vx=0;
		}
		if (y<0)
		{
			y=0;
			vy=0;
		}
		if (y> HEIGHT-getHeight())
		{
			y=HEIGHT-getHeight();
			vy=0;
		}
	}
	public void shoot()
	{
		//..bullet.shoot(getPosition(),playerDirect);
		System.out.println(shoot);
		bullets.add(new Bullet(getPosition(),getPlayerDirect(),true));
		shoot = true;
	}
	public void drawBullets(SpriteBatch sb)
	{
		for (int i = 0 ; i < bullets.size() ; i++)
		{
			bullets.get(i).getSprite().draw(sb);;
		}
	}
	public Rectangle getBoundsBullet(int id)
	{
		if (bullets.get(id)!= null)
		{
			return bullets.get(id).getBounds();
		}
		return new Rectangle();
	}
	public void removeBullet(int id)
	{
		if (bullets.get(id)!= null)
		{
			bullets.clear();
			setShoot(false);
		
		}
	}


	
	
	
	
	public direction getPlayerDirect() {
		return playerDirect;
	}

	public void setPlayerDirect(direction playerDirect) {
		this.playerDirect = playerDirect;
	}

	public Vector2 getPosition() {
		// TODO Auto-generated method stub
		return new Vector2(x,y);
	}
	public boolean isShooting() {
		// TODO Auto-generated method stub
		
		return shoot;
	}

	
	
}
