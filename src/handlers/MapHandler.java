package handlers;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import Packet.PacketTile;
import ServerGame.MapGame;
import Tiles.Tile;

public class MapHandler {

	ArrayList<Tile> tileMap = new ArrayList<Tile>();
	 public MapHandler(MapGame map) {
		// TODO Auto-generated constructor stub
		 for (int i=0 ; i <  map.size(); i++)
		 {
			tileMap.add(new Tile(map.getTile(i)));
		 }
	}
	 
	 public ArrayList<Tile> getTileMap()
	 {
		 return tileMap;
	 }
	 public void update(MapGame map)
	 {
		PacketTile packet = map.getUpdateTile();
		System.out.println("update "+ packet.size());
		 for(int i=0; i < packet.size() ; i++)
		 {
			 System.out.println("zmiana "+i + " " + packet.getIDs().get(i));
			tileMap.set(packet.getIDs().get(i), packet.getTiles().get(i));
		 }
	 }
	 public void draw(SpriteBatch sb)
	 {
		for (int i=0 ; i < tileMap.size() ; i++)
		 {
			 tileMap.get(i).draw(sb);
		 }
	 }
}
