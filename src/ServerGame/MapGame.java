package ServerGame;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import Packet.PacketTile;
import Tiles.Tile;
import XMLPraser.ReaderXML;

public class MapGame implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4001699460120474088L;
	// 20x15
	private ArrayList<Tile> mapLoad;
	private ReaderXML levelLoaded = new ReaderXML("levels/level2.xml");

	private ArrayList<Integer> updateTile = new ArrayList<Integer>();

	public MapGame() {
		// TODO Auto-generated constructor stub
		try {
			mapLoad = levelLoaded.loader(false);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Vector2 getSpawnPosition(int id) {
		return levelLoaded.getSpawn(id);
	}

	public ArrayList<Tile> getMap() {
		return mapLoad;
	}

	public PacketTile getUpdateTile() {
		PacketTile packetTile = new PacketTile();

		for (int i = 0; i < updateTile.size(); i++) {
			packetTile = new PacketTile(updateTile.get(i),
					mapLoad.get(updateTile.get(i)));

		}
		updateTile.clear();
		return packetTile;
	}

	public int size() {
		return mapLoad.size();
	}

	public Tile getTile(int ID) {
		return mapLoad.get(ID);
	}

	public void setTile(Tile tile, int id) {
		mapLoad.set(id, tile);
		updateTile.add(id);
	}

	public void setTile(Rectangle bounds, int type, String option, int id) {
		// TODO Auto-generated method stub
		System.out.println("typ  " + type);
		mapLoad.set(id, new Tile(bounds, type, option));
		updateTile.add(id);
	}

}
