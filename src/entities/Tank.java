package entities;

import java.util.ArrayList;
import java.util.Random;


import handlers.BotEnemy;
import AssetsLoader.Assets;
import Tiles.Tile;
import Utils.*;
import Utils.Direction.direction;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;


public class Tank extends Entity {
	
	private Vector2 playerPos;
	private Vector2 futurePos;
	private BotEnemy bot;
	private boolean moveBlock = false;
	private direction d;
	private ArrayList<Tile> tileMap = new ArrayList<Tile>();
	private ArrayList<TankUtil> tanks = new ArrayList<TankUtil>();
	private ArrayList<Bullet> bullets = new ArrayList<Bullet>();
	private Rectangle rPlayer;

	private boolean shooted =false;
	private boolean isCollide = false;
	
	public Tank(Rectangle rPlayer,Vector2 startPos)
	{
		Texture text = Assets.getManager().get("assets/texture/enemy_tank_1.png", Texture.class);
		sprite = new Sprite(text);
		setPosition(startPos.x,startPos.y);

		x = startPos.x;
		y = startPos.y;
		playerPos = new Vector2(0,0);
		futurePos = new Vector2(x,y);
		
		
		setDirection(direction.Up);
		bounds = new Rectangle(getX(),getY(),getWidth(),getHeight());
		this.rPlayer= rPlayer;
	}

	
	public void setDirection(Direction.direction direction)
	{
		this.d = direction;
	}

	public direction getDirection()
	{
		return this.d;
	}
	public Sprite getSprite() {
		return sprite;
	}
	public void updateMap(ArrayList<Tile>tileMap)
	{
		this.tileMap = tileMap;
	}

	
	public void moveTo(float vx , float vy)
	{
	
		float oldX=x,oldY=y;
		
		boolean collisionX = false,collisionY = false;
		if (x == vx)
		{
			if (y == vy)	
				moveBlock = false;
		}

		if ( x != vx || y!= vy  )
		{
			for (int i = 0 ;i < tileMap.size();i++ )
			{
				if (bounds.overlaps(tileMap.get(i).getBounds()) && (tileMap.get(i).getOptions().equals("block") || tileMap.get(i).getOptions().equals("shotable")))
				{
				//	System.out.println("kolizja " + i);
					backMove(vx, vy);
					moveBlock = false;
					
					isCollide = true;
				}
				else
					isCollide = false;
			}
			for (int i = 0 ;i < tanks.size(); i++)
			{
				if (bounds.overlaps(tanks.get(i).getBounds()))
				{
					System.out.println("kolizja tanki " + i);
					backMove(vx, vy);
					moveBlock = false;
					
					
					isCollide = true;
				}
				else
					isCollide = false;
			}
			if(isCollide==false)
			{
			if (isLeft(vx))
				{
						x -=1;
						setDirection(Direction.direction.Left);
						sprite.setRotation(90);
						isCollide = false;
				}
			else if (isRight(vx))
				{
						x +=1;
						setDirection(Direction.direction.Right);
						sprite.setRotation(-90);
						isCollide = false;
				}
			else if (isTop(vy))
				{
						y+=1;
						setDirection(Direction.direction.Up);
						sprite.setRotation(0);
						isCollide = false;
				}
			else if(isBot(vy))
				{
						y -=1;
						setDirection(Direction.direction.Down);
						sprite.setRotation(180);
						isCollide = false;
				}
				
			}
		}	
	}
	private void changeMove(Direction.direction dir)
	{
		switch (dir) {
		case Left:
				
			break;

		default:
			break;
		}
	}
	private void backMove(float vx, float vy)
	{
	
		if (isLeft(vx))
			x+=2;
		if(isRight(vx))
			x -=2;
		if(isTop(vy))
			y -=2;
		if(isBot(vy))
			y+=2;

	}
	private boolean isLeft(float vx)
	{
		if (x > vx )
			return true;
		return false;
	}
	private boolean isRight(float vx)
	{
		if (x < vx)
			return true;
		return false;
	}
	
	private boolean isTop(float vy)
	{
		if (y < vy)
			return true;
		return false;
	}
	private boolean isBot(float vy)
	{		if (y > vy)
			return true;
		return false;
	}
	
	
	
	public void update(Vector2 pos,Rectangle rPlayer,ArrayList<TankUtil> tanks)
	{
		playerPos = pos;
		this.tanks = tanks;
			

			for(int i=0; i < bullets.size() ;i++)
			{
				for (int j=0; j < tileMap.size(); j++)
				{
					if(bullets.get(i).getBounds().overlaps(new Rectangle(50,50,50,50)))
					{
						System.err.println("kolizja");
					}
				bullets.get(i).update(new Vector2(x, y));	
				}
			}	
			
		
			
			
			if (x < 0 )
			{
				x=0;
				moveBlock= false;
			}
			if(x>WIDTH-getWidth())
			{
				x=WIDTH-getWidth();
				moveBlock= false;
			}
			if (y<0)
			{
				y=0;
				moveBlock= false;
			}
			if (y> HEIGHT-getHeight())
			{
				y=HEIGHT-getHeight();
				moveBlock= false;
			}

		this.rPlayer = rPlayer;
		sprite.setPosition(x, y);
		bounds.setX(x);
		bounds.setY(y);
		makeDecision();
		clean();
	}
	
	private void clean()
	{
		for (int i = 0 ; i< bullets.size() ; i++)
		{
			if(bullets.get(i).getPositon().x <0) 
			{
				System.out.println("usuwa " + i);
				bullets.remove(i);
				break;
			}
			else if(bullets.get(i).getPositon().x > 640) 
				{
					System.out.println("usuwa " + i);
					bullets.remove(i);
					break;
				}
		/*	else	if(bullets.get(i).getPositon().y <0)
					{
						System.out.println("usuwa " + i);
						bullets.remove(i);
						break;
					}
			else 		if(bullets.get(i).getPosition().y > 480)
						{
							System.out.println("usuwa " + i);
							bullets.remove(i);
							break;
						}
			*/	
		
		}
	}
	public Rectangle getBounds()
	{
		return bounds;
	}

	public boolean isMoveBlock() {
		return moveBlock;
	}

	public void setMoveBlock(boolean moveBlock) {
		this.moveBlock = moveBlock;
	}
	private void setNext(Vector2 pos )
	{
		futurePos = pos;
	}
	
	public void makeDecision() 
	{
		//System.out.println(futurePos.x + " " +futurePos.y);
		
		if (!moveBlock)
		{
			float decision = randomState()*100 ;/// 100;
			
			if (decision <= 60f)
			{
				float tmp = decision;
				if ((tmp >= 0 && tmp < 15f) )
				{
					//System.out.println("lewo");
					setNext(new Vector2(x-32*random(),y)); //left
					moveBlock = true;
				}
				else if (tmp >= 15.0f && tmp <30.0f)
				{
				//	System.out.println("prawo");
					setNext(new Vector2(x+32*random(),y));//right
					moveBlock = true;
				}
				else if (tmp >=30.0f && tmp < 45.0f)
				{
				//	System.out.println("top");
					setNext(new Vector2(x, y+32*random())); //top
					moveBlock = true;
				}
				else if (tmp >=45.0f && tmp < 60.0f)
				{
				//	System.out.println("dol");
					setNext(new Vector2(x, y-32*random())); //bot
					moveBlock = true;
				}
				
				//System.out.println(futurePos.x + " " + futurePos.y);
			}
			else if (decision>60f)
			{
				if (decision>90.0)
				{
				System.out.println("Strzelam tank" + getPosition().x + " " + getPosition().y);
				shooted= true;
				bullets.add(new Bullet(new Vector2(x, y), getDirection(), shooted));
				}
			}
		}
		if (moveBlock)
		{
			moveTo(futurePos.x, futurePos.y);
		}
	}
	public void drawBullets(SpriteBatch sb)
	{
		for (int i=0; i < bullets.size(); i++)
		{
		//	System.err.println("draw bullets" + i);
			bullets.get(i).getSprite().draw(sb);
		}
	}
	
	private int possibleMove()
	{
		
		Random random = new Random();
		int move = random.nextInt(3) ;
		if(move != changeToInt(getDirection()))
		{
			return move;
		}
		
		return 0;
	}
	private int changeToInt(Direction.direction direction)
	{
		switch (direction) {
		case Left:
			return 0;
		case Right:
			return 1;
		case Up:
			return 2;
		case Down:
			return 3;
		default:
			return -1;
		}
		
		
	}
	private int random() {
		Random rand = new Random();
		return rand.nextInt(10);
	}
	public float randomState()
	{
		Random rand = new Random();
		return rand.nextFloat();
	}
}
