package Packet;

import java.io.Serializable;


import com.badlogic.gdx.math.Rectangle;

import Tiles.Tile;

public class PacketTileUpdate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2742564127651262004L;
	
	private Rectangle bounds;
	private int type;
	private int ID;
	private String option;
	public PacketTileUpdate(Tile tile,int ID) {
		// TODO Auto-generated constructor stub
		this.bounds = tile.getBounds();
		this.type = tile.getType();
		this.option = tile.getOptions();
		this.ID = ID;
	}

	public Rectangle getBounds()
	{
		return bounds;	 
	}
	public int getType()
	{
		return type;
	}
	public String getOption()
	{
		return option;
	}
	public int getID()
	{
		return ID;
	}
	
}
