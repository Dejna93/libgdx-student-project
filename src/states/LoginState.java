package states;

import handlers.DatebaseHandler;
import handlers.GameStateManager;




import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;

import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;


import network.Connection;

public class LoginState extends GameState {
	
	Skin skin;
	Stage stage;

	private Table table;
	
	private TextButton loginButton;
	private TextButton registerButton;
	private TextField userField;
	private TextField passwordField;
	private TextField addressField;
	private TextField portField;
	private DatebaseHandler db;
	private Label userLabel;
	private Label passLabel;
	private Label addressLabel;
	private Label portLabel;
	private Connection connection;
	public LoginState(GameStateManager gSManager) {
		super(gSManager);
		
		
		System.err.println("new client running");
		// TODO Auto-generated constructor stub
		stage = new Stage();
		connection = new Connection();
		skin = new Skin(Gdx.files.internal("assets/json/uiskin.json"));
		
		db = new DatebaseHandler();
		
		loginButton = new TextButton("Logowanie", skin , "default");
		registerButton = new TextButton("Rejestracja",skin,"default");
		
		addressField = new TextField("",skin);
		portField = new TextField("7777", skin);
		userField  = new TextField("", skin);
		passwordField = new TextField("", skin);
		
		passwordField.setPasswordMode(true);
		passwordField.setPasswordCharacter('*');

		
		userLabel = new Label("Username",skin);
		passLabel = new Label("Password",skin);
		addressLabel = new Label("Server IP", skin);
		portLabel = new Label("Port Server",skin);
		table = new Table();
		table.setFillParent(true);
		
		generate();
		login();
		register();
        Gdx.input.setInputProcessor(stage);// Make the stage consume events
      
       
        stage.addActor(table);
      //  table.setDebug(true);
	
      
	}
	private void generate()
	{
		table.center().center();
		table.add(addressLabel);
		table.add(portLabel).left().width(50);
		table.row();
		table.add(addressField).width(200).pad(20);
		table.add(portField).width(80).left();
		

		table.row();
		table.add(userLabel);
		table.add(userField).width(200);
		table.row();
		table.add(passLabel);
		table.add(passwordField).width(200);
		table.row();
		table.add(loginButton);
		table.add(registerButton);	
		
		stage.addActor(table);
	}
	private void login()
	{
		
		loginButton.addListener(new ChangeListener() {
			
	    	   public void changed (ChangeEvent event, Actor actor) {
	    		   
	    		   connection = new Connection(checkAddress(addressField.getText()), Integer.parseInt(portField.getText()));
	    		   
	    		   String a = (String)userField.getText().trim();
	    		   String b =(String)passwordField.getText().trim();
	    		   

	    		connection.sendLogin(a,b);	
	    		
	    		
	    		
	    		if (connection.isLogin() == true)
	    		{
	    			gSManager.pushState(gSManager.MULTIPLAYER,connection);//MULTIPLAYER,connection);
	    		}
	    		/*else
	    		{		
	    			stage.clear();
	    			Window window = new Dialog("Blad logowania",skin);
	    			Button accept = new TextButton("Accept",skin);
	    				//window.setMovable(false);
	    			window.setPosition(Gdx.graphics.getWidth()/2 - Gdx.graphics.getWidth()/6 , Gdx.graphics.getHeight()/2);
	    			window.setWidth(200);
	    			window.setHeight(100);
	    			
	    			window.add(accept);
	    				
	    			window.setTitle("Error Login");
	    			
	    			window.addListener(new ChangeListener() {
	    			    	   public void changed (ChangeEvent event, Actor actor) {
	    				    		stage.clear();
	    				    		generate();
	    			    	   }
	    			});
	    				
	    			stage.addActor(actor);
	    			stage.addActor(window);

	    			}*/
	    			
	   
	    		}
	       });
	}
	private void register()
	{
		registerButton.addListener(new ChangeListener() {
			
	    	   public void changed (ChangeEvent event, Actor actor) {
	    		 gSManager.popState();
	    		 gSManager.pushState(gSManager.REGISTER);
	    		}
	       });
	}

	private String checkAddress(String address)
	{
		if(address.length() == 0)
		{
			return new String("localhost");
		}
		return address;
			
	}
	@Override
	public void handleInput() {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		if(Gdx.input.isKeyPressed(Keys.ESCAPE))
			{
			connection.sendDisconnect(true);
			gSManager.popState();
			gSManager.pushState(gSManager.MENU);
		}
		if (connection.isLogin() == true)
		{
			gSManager.pushState(gSManager.MULTIPLAYER,connection);//MULTIPLAYER,connection);
		}
	
		
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		
		stage.act();
		stage.draw();
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
	public Label getUserLabel() {
		return userLabel;
	}
	public void setUserLabel(Label userLabel) {
		this.userLabel = userLabel;
	}
	public Label getPassLabel() {
		return passLabel;
	}
	public void setPassLabel(Label passLabel) {
		this.passLabel = passLabel;
	}

}
