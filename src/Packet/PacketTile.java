package Packet;

import java.io.Serializable;
import java.util.ArrayList;

import Tiles.Tile;

public class PacketTile implements Serializable {

	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 4206627668937282343L;
	/**
	 * 
	 */
	
	private ArrayList<Tile> tileMap = new ArrayList<Tile>();
	private ArrayList<Integer> idTile = new ArrayList<Integer>();

	public PacketTile() {
		// TODO Auto-generated constructor stub
	}
	
	public PacketTile(int id,Tile tile) {
		// TODO Auto-generated constructor stub
		tileMap.add(new Tile(tile));
		idTile.add(id);
		System.out.println(id + " ");
	}
	public int size()
	{
		if (tileMap.size() == idTile.size())
			return tileMap.size();
		else
			return 0;
	}
	public ArrayList<Tile> getTiles()
	{
		return tileMap;
	}
	public ArrayList<Integer> getIDs()
	{
		return idTile;
	}
}
