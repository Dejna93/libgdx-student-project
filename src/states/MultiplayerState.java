package states;


import java.util.ArrayList;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;

import com.badlogic.gdx.math.Vector2;



import Tiles.Tile;
import entities.BulletMulti;
import entities.PlayerMulti;
import handlers.BulletsHandler;
import handlers.GameStateManager;
import handlers.MapHandler;
import handlers.PlayersHandler;
import network.Connection;

public class MultiplayerState extends GameState {

	private OrthographicCamera cam;

	private Connection connection;
	// private ArrayList<PlayerMulti> players = new ArrayList<PlayerMulti>();
	private PlayerMulti player;
	private ArrayList<Tile> tileMap = new ArrayList<Tile>();
	private PlayersHandler enemies = new PlayersHandler();
	private BulletsHandler bullets = new BulletsHandler();

	private MapHandler map;

	private long startTime;

	float  shootTime =0 ;
	float shootDelay   = 1f;
	public MultiplayerState(GameStateManager gSManager) {
		super(gSManager);
		// TODO Auto-generated constructor stub

	}

	public MultiplayerState(GameStateManager gSManager, Connection connection) { // for
																					// login
		super(gSManager);
		startTime = System.currentTimeMillis();
		// TODO Auto-generated constructor stub
		this.connection = connection;
		// enemy.setPosition(400, 400);
		// enemies = new PlayersHandler(connection.getPlayers(),
		// connection.getPort());
		player = new PlayerMulti();

		map = new MapHandler(connection.getMap());

		cam = new OrthographicCamera();
		cam.setToOrtho(false, WIDTH, HEIGHT);
		cam.update();
		cam.position.set(WIDTH / 2, HEIGHT / 2, 0);

		
	
		// enemies = new PlayersHandler(connection.getPlayers(),
		// connection.getID());

	}

	@Override
	public void handleInput() {
		// TODO Auto-generated method stub
		/*
		 * if(Gdx.input.isKeyPressed(Keys.ESCAPE)) {
		 * System.out.println("koniec"); connection.sendDisconnect(true);
		 * gSManager.popState(); gSManager.pushState(gSManager.MENU); }
		 */

		if (Gdx.input.isKeyPressed(Keys.UP)) {
			connection.sendMove(new Vector2(0, 2));
			// player.move(connection.getMove());
		} else if (Gdx.input.isKeyPressed(Keys.DOWN)) {

			connection.sendMove(new Vector2(0, -2));

		} else if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			connection.sendMove(new Vector2(-2, 0));

		} else if (Gdx.input.isKeyPressed(Keys.RIGHT)) {

			connection.sendMove(new Vector2(2, 0));

		}

		if (Gdx.input.isKeyPressed(Keys.SPACE)  && shootTime > shootDelay  ) // && lastShoot > 0.2f)
		{
			// System.err.println(player.getPosition().x + " " +
			// player.getPosition().y);
			connection.sendShoot(new BulletMulti(connection.getID(), player
					.getPosition(), player.getPlayerDirect()));
			shootDelay +=1f;
		}
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		handleInput();
		player.update(connection.getPlayer(connection.getID()));

		map.update(connection.getMap());

		/*
		 * for (int i =0 ; i < connection.getPlayers().size() ; i++ ) {
		 * if(connection.getPlayers().get(i).getID() != connection.getPort())
		 * enemy.setPosition(connection.getPlayer(i).getPosition().x,
		 * connection.getPlayer(i).getPosition().y);
		 * //System.err.println("nie jestem" + i);
		 * 
		 * }
		 */
		bullets.update(connection.getBullets());
		// enemies.update(connection.getPlayers(), connection.getPort());
		enemies.update(connection.getPlayers(), connection.getID());

		cam.update();
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		draw();
		 System.err.println("Time left " +(System.currentTimeMillis() -  startTime) / 1000);
		shootTime = (System.currentTimeMillis() -  startTime) / 1000;
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		sb.begin();
		map.draw(sb);
		player.getSB().draw(sb);
		// enemy.draw(sb);
		enemies.draw(sb);

		if (bullets != null)
			bullets.draw(sb);
		// enemies.draw(sb);
		/*
		 * for (int i=0; i < players.size() ; i++) {
		 * players.get(i).getSB().draw(sb); }
		 */

		sb.end();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		connection.sendDisconnect(true);
	}

}
