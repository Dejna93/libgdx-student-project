package entities;

import com.badlogic.gdx.graphics.g2d.Sprite;



public abstract class Enemy {

	protected float x , y;
	protected boolean dead;
	protected int health;
	
	protected int width , height;
	protected Sprite sprite;
	
	public boolean isDead() { return dead;}
	
	public void hit()
	{
		health -= 1;
		if (health <= 0)
		{
			dead = true;
		}
	}
	
	
	
}
